# Binaural Beats

(Based on https://github.com/GiorgioRegni/Binaural-Beats)

Binaural Beats meditation helper. To create custom binaural beats programms please use Gnaural application, links below.

# Manual install

    gradle installDebug

# Translate

If you want to translate 'Binaural Beats' to your language  please read this:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

![shot](/docs/shot.png)

# Links

  * http://gnaural.sourceforge.net/
