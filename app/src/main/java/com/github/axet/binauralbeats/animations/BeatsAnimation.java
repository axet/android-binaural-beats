package com.github.axet.binauralbeats.animations;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.github.axet.androidlibrary.animations.ExpandAnimation;
import com.github.axet.androidlibrary.animations.MarginAnimation;
import com.github.axet.binauralbeats.R;

public class BeatsAnimation extends ExpandAnimation {
    View bottom_ctr;

    View button_loop;
    View button_minus;
    View button_plus;
    View button_play;
    View button_stop;

    boolean playing;

    public static Animation apply(final RecyclerView list, final View v, final boolean expand, boolean animate, final boolean playing) {
        return apply(new LateCreator() {
            @Override
            public MarginAnimation create() {
                return new BeatsAnimation(list, v, expand, playing);
            }
        }, v, expand, animate);
    }

    public BeatsAnimation(RecyclerView list, View v, boolean expand, boolean playing) {
        super(list, v, v.findViewById(R.id.recording_player), v.findViewById(R.id.beats_expand), expand);
        this.playing = playing;
        bottom_ctr = v.findViewById(R.id.beats_controls);
        button_minus = v.findViewById(R.id.button_minus);
        button_plus = v.findViewById(R.id.button_plus);
        button_loop = v.findViewById(R.id.button_loop);
        button_play = bottom_ctr.findViewById(R.id.beats_player_play);
        button_stop = bottom_ctr.findViewById(R.id.beats_player_stop);
    }

    public void init() {
        super.init();
        button_plus.setVisibility(View.VISIBLE);
        button_minus.setVisibility(View.VISIBLE);
        button_play.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
        button_stop.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
        button_loop.setVisibility(View.VISIBLE);
    }

    @Override
    public void calc(final float i, Transformation t) {
        super.calc(i, t);

        float ii = expand ? i : 1 - i;
        float k = expand ? 1 - i : i;

        ViewCompat.setAlpha(button_plus, ii);
        ViewCompat.setAlpha(button_minus, ii);
        if (playing) {
            ViewCompat.setAlpha(button_play, k);
            ViewCompat.setAlpha(button_stop, k);
        } else {
            ViewCompat.setAlpha(button_loop, ii);
        }
    }

    @Override
    public void restore() {
        super.restore();
        ViewCompat.setAlpha(button_plus, 1);
        ViewCompat.setAlpha(button_minus, 1);
    }

    @Override
    public void end() {
        super.end();
        if (expand) {
            button_plus.setVisibility(View.VISIBLE);
            button_minus.setVisibility(View.VISIBLE);
            button_play.setVisibility(View.INVISIBLE);
            button_stop.setVisibility(View.INVISIBLE);
            button_loop.setVisibility(View.VISIBLE);
        } else {
            button_plus.setVisibility(View.INVISIBLE);
            button_minus.setVisibility(View.INVISIBLE);
            button_play.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
            button_stop.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
            button_loop.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
        }
        view.setVisibility(expand ? View.VISIBLE : View.GONE);
    }
}
